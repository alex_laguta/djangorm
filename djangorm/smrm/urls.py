from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'trash/(?P<trash_ID>[0-9]+)/$', views.trash, name='trash'),
    url(r'trash/(?P<trash_ID>[0-9]+)/edit/$', views.edit_trash, name='edit_trash'),
    url(r'trash/add/$', views.add_trash, name='add_trash'),
    url(r'tasks/$', views.tasks, name='tasks'),
    url(r'task/(?P<task_ID>[0-9]+)/$', views.task, name='task'),
    url(r'task/add/$', views.add_task, name='add_task'),
    url(r'.+$', views.error, name='error'),
]
