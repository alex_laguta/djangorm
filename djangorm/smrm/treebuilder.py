import json
import os


def build_path_dict(path):
    dict = {'text': os.path.basename(path), 'id': os.path.abspath(path)}
    if os.path.isdir(path):
        dict['type'] = "directory"
        dict['children'] = [build_path_dict(os.path.join(path, x)) for x in os.listdir(path)]
    else:
        dict['type'] = "file"
    return dict


def json_tree():
    return json.dumps([build_path_dict('/home/student/student')])
