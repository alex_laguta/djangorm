# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-30 18:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('smrm', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='regex',
            field=models.CharField(blank=True, default='', max_length=200),
        ),
    ]
