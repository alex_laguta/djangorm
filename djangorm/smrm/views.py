# -*- coding: utf-8 -*-
import json

from django.shortcuts import render, redirect
from django.http import HttpResponseNotFound
from models import Trash, Task, File
from forms import TrashForm, TaskForm, TrashEditForm
from treebuilder import json_tree
from src import smrm
from src.trash import Trash as TrashRM
from src.smrmexceptions import RMException


def index(request):
    trashes = Trash.objects.all()
    return render(request, 'smrm/index.html', {'trashes':trashes})


def trash(request, trash_ID):
    chosen_trash = Trash.objects.get(pk = trash_ID)
    tasks = chosen_trash.task_set.all()
    if request.GET.get("clear"):
        chosen_trash.clear_trash()
        for trash_task in tasks:
            if trash_task.trash == chosen_trash:
                trash_task.delete()
        return redirect('trash', chosen_trash.id)
    if request.GET.get("delete"):
        chosen_trash.clear_trash()
        for trash_task in tasks:
            if trash_task.trash == chosen_trash:
                trash_task.delete()
        chosen_trash.delete()
        return redirect('index')
    if request.GET.get('edit'):
        return redirect('edit_trash', chosen_trash.id)
    return render(request, 'smrm/trash.html', {'trash': chosen_trash, 'tasks': tasks})


def add_trash(request):
    if request.method == 'POST':
        form = TrashForm(request.POST)
        if form.is_valid():
            form.save()
        trashes = Trash.objects.all()
        return render(request, 'smrm/index.html', {'trashes': trashes})
    else:
        form = TrashForm()
        return render(request, 'smrm/add_trash.html', {'form': form})


def edit_trash(request, trash_ID):
    chosen_trash = Trash.objects.get(pk=trash_ID)
    tasks = chosen_trash.task_set.all()
    if request.method == 'POST':
        chosen_trash.name = request.POST['name']
        chosen_trash.policy = request.POST['policy']
        if request.POST.has_key('auto_deletion'):
            if request.POST['auto_deletion'] == 'on':
                chosen_trash.auto_deletion = 'True'
        else:
            chosen_trash.auto_deletion = 'False'
        chosen_trash.auto_space = request.POST['auto_space']
        chosen_trash.auto_time = request.POST['auto_time']
        chosen_trash.save(update_fields=['name', 'policy', 'auto_deletion', 'auto_space', 'auto_time'])
        return render(request, 'smrm/trash.html', {'trash': chosen_trash, 'tasks': tasks})
    else:
        form = TrashEditForm(instance=chosen_trash)
        return render(request, 'smrm/edit_trash.html', {'form': form})


def task(request, task_ID):
    chosen_task = Task.objects.get(pk=task_ID)
    files = chosen_task.file_set.all()
    trash_tasks = chosen_task.trash.task_set.all()
    if chosen_task.transmission:
        operation = TrashRM(chosen_task.trash.path).get_transmission(chosen_task.transmission)
    else:
        operation = None
    if request.GET.get('run'):
        try:
            if chosen_task.status == Task.WAITING:
                return redirect('task', chosen_task.id)
            chosen_task.trash.in_progress = True
            chosen_task.trash.save(update_fields=['in_progress'])
            for trash_task in trash_tasks:
                trash_task.previous_status = trash_task.status
                trash_task.status = Task.WAITING
                trash_task.save(update_fields=['previous_status', 'status'])

            tr = TrashRM(chosen_task.trash.path)
            if chosen_task.trash.auto_deletion:
                if chosen_task.trash.auto_time > 0:
                    tr.maxtime = chosen_task.trash.auto_time
                if chosen_task.trash.auto_space > 0:
                    tr.maxsize = chosen_task.trash.auto_space
            if chosen_task.regex != '':
                transmission = smrm.multi_remove(tr, chosen_task.file_set.all(),
                                                 chosen_task.regex, chosen_task.dry_run)
            else:
                transmission = smrm.multi_remove(tr, chosen_task.file_set.all(), None, chosen_task.dry_run)
            if chosen_task.trash.auto_deletion:
                tr.auto_clear(transmission)
            if transmission is not None:
                chosen_task.transmission = transmission

            chosen_task.previous_status = Task.OK
            operation = tr.get_transmission(transmission)
            chosen_task.save(update_fields=['previous_status', 'transmission'])
        except RMException as exc:
            chosen_task.previous_status = Task.ERROR
            chosen_task.error_message = exc.message
            chosen_task.save(update_fields=['previous_status', 'error_message'])
        except Exception:
            chosen_task.previous_status = Task.ERROR
            chosen_task.save(update_fields=['previous_status'])
        finally:
            trash_tasks = chosen_task.trash.task_set.all()
            for trash_task in trash_tasks:
                trash_task.status = trash_task.previous_status
                trash_task.save(update_fields=['status'])
            chosen_task.status = chosen_task.previous_status
            chosen_task.save(update_fields=['status'])
            chosen_task.trash.in_progress = False
            chosen_task.trash.save(update_fields=['in_progress'])
            return render(request, 'smrm/task.html', {'task': chosen_task, 'files': files, 'operation': operation})
    if request.GET.get('restore'):
        try:
            if chosen_task.status == Task.WAITING:
                return redirect('task', chosen_task.id)
            if not chosen_task.transmission:
                raise RMException('Transmission not found')
            tr = TrashRM(chosen_task.trash.path)
            smrm.restore(chosen_task.transmission, tr,
                         dry_run=chosen_task.dry_run, choice_policy=chosen_task.trash.policy)
            chosen_task.status = Task.READY
        except RMException as exc:
            chosen_task.status = Task.ERROR
            chosen_task.error_message = exc.message
            chosen_task.save(update_fields=['error_message'])
        except Exception:
            chosen_task.status = Task.ERROR
        finally:
            chosen_task.save()
            return redirect('task', chosen_task.id)
    if request.GET.get('delete'):
        chosen_task.delete()
        return redirect('index')
    return render(request, 'smrm/task.html', {'task': chosen_task, 'files': files, 'operation': operation})


def add_task(request):
    if request.method == 'POST':
        instance = None
        form = TaskForm(request.POST)
        if form.is_valid():
            instance = form.save()
        tasks = Task.objects.all()
        files = json.loads(request.POST.get('files'))
        for file in files:
            new_file_model = File(path=file, task=instance)
            new_file_model.save()
        chosen_task = Task.objects.get(name=request.POST['name'])
        if chosen_task.trash.in_progress:
            chosen_task.status = Task.WAITING
            chosen_task.save(update_fields=['status'])
        return render(request, 'smrm/tasks.html', {'tasks': tasks})
    else:
        form = TaskForm()
        js_tree = json_tree()
        return render(request, 'smrm/add_task.html', {'form': form, 'json_tree': js_tree})


def tasks(request):
    tasks = Task.objects.all()
    return render(request, 'smrm/tasks.html', {'tasks': tasks})


def error(request):
    return HttpResponseNotFound('NOT FOUND!')