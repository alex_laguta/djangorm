import os
import csv
import shutil
import logging
import pickle
from datetime import datetime

from src.smrmexceptions import *
from src.file import File


class Trash(object):
    def __init__(self, path=None):
        """
        Trash initialization
        """
        self.logger = logging.getLogger('SmartRM_logger')
        self.logger.debug('Trash initialization ...')

        if not path:
            path = os.path.expanduser('~/trash')
        self.trash_path = path
        self.trash_files_path = os.path.join(path, 'files/')
        if not os.path.exists(self.trash_files_path):
            os.makedirs(self.trash_files_path)
        self.trash_info_path = os.path.join(path, 'info/')
        if not os.path.exists(self.trash_info_path):
            os.makedirs(self.trash_info_path)

        self.info_file = os.path.join(self.trash_info_path, 'info.csv')
        self.info_file_header = ['Name', 'Trash name', 'Size', 'Deletion time', 'Restore path']
        if not os.path.exists(self.info_file):
            with open(self.info_file, 'w') as file_with_info:
                writer = csv.DictWriter(file_with_info, self.info_file_header)
                writer.writeheader()

        self.path_transmissions = None
        self.transmissions = None
        self.init_transmissions()
        self.maxsize = None
        self.maxtime = None
        self.given_space = None

        self.logger.debug('Trash was initialized successfully')

    def move_to_trash(self, path, lock):
        """
        Move the file to the trash using its path
        """
        self.logger.info(path + ' trying to move to trash')
        if not os.path.exists(path):
            raise RMException(path + 'File not found')

        if path.startswith(self.trash_path):
            raise RMException('Try to delete files in trash')

        if os.path.isdir(path):
            if os.listdir(path):
                raise RMException(path + ' directory is not empty')
            if not os.access(path, os.R_OK):
                raise RMException(path + ' permission denied')

        file = File(path)
        filename_in_trash = file.name
        path_to_move = os.path.join(self.trash_files_path, filename_in_trash)
        if os.path.exists(path_to_move):
            same_names_counter = 2
            while os.path.exists(path_to_move + '.' + str(same_names_counter)):
                same_names_counter += 1
            path_to_move = path_to_move + '.' + str(same_names_counter)
            filename_in_trash = filename_in_trash + '.' + str(same_names_counter)

        info = {'Name': file.name,
                'Trash name': filename_in_trash,
                'Size': file.get_size(),
                'Deletion time': file.get_last_modified_time(),
                'Restore path': file.abspath}

        lock.acquire()
        try:
            with open(self.info_file, 'a') as file_with_info:
                writer = csv.DictWriter(file_with_info, self.info_file_header)
                writer.writerow(info)
        finally:
            lock.release()

        os.rename(file.abspath, path_to_move)

        self.logger.info(path + ' moved to trash successfully')

        return filename_in_trash

    def restore(self, filename):
        """
        Restore the file using its name in the trash
        """
        self.logger.info(filename + ' trying to restore from trash')

        with open(self.info_file, 'r') as file_with_info:
            reader = csv.DictReader(file_with_info)
            file_info = [row for row in reader]

        file_info_to_restore = filter(lambda info: info['Trash name'] == filename, file_info)
        if not file_info_to_restore:
            raise RMException('File not found')
        restore_info = file_info_to_restore[0]
        self._restore_by_info(restore_info)
        file_info.remove(restore_info)

        with open(self.info_file, 'w') as f:
            writer = csv.DictWriter(f, self.info_file_header)
            writer.writeheader()
            writer.writerows(file_info)

        self.logger.info(filename + ' restored successfully')

    def show(self, only_names=False):
        """
        Show all the files that are in the trash
        """
        with open(self.info_file, 'r') as file_with_info:
            reader = csv.DictReader(file_with_info)
            file_info = [row for row in reader]

        for info in file_info:
            filename = info.get('Name')
            if not only_names:
                name_in_trash = info.get('Trash name')
                path_to_restore = info.get('Restore path')
                file_size = info.get('Size')
                time = info.get('Deletion time')
                print('{:<10} {:<10} {:<40} {:<10} {:<20}'.format(filename,
                                                                  name_in_trash, path_to_restore, file_size, time))
            else:
                print(filename)

    def clear_trash(self):
        """
        Clear trash
        """
        self.logger.info('Clearing the trash')

        with open(self.info_file, 'r') as csv_file:
            reader = csv.DictReader(csv_file)
            file_info = [row for row in reader]
            for info in file_info:
                self._remove_file_from_trash(info['Trash name'], None)
        self._delete_files_info()

        self.logger.info('The trash was cleared')

    def auto_clear(self, transmission_id):
        """
        Clear trash using the autoclear policies
        """
        transmission = self.get_transmission(transmission_id)
        with open(self.info_file, 'r') as file_with_info:
            reader = csv.DictReader(file_with_info)
            files_info = [row for row in reader]

        if self.given_space is not None:
            self._delete_by_space(files_info, transmission)
        elif self.maxsize is not None:
            self._delete_by_size(files_info, transmission)
        elif self.maxtime is not None:
            self._delete_transmissions_by_time(files_info)

    def _remove_file_from_trash(self, filename, transmission):
        """
        Delete the file from the trash
        """
        with open(self.info_file, 'r') as file_with_info:
            reader = csv.DictReader(file_with_info)
            file_info = [row for row in reader]

        file_info_to_remove = filter(lambda info: info['Trash name'] == filename, file_info)
        if not file_info_to_remove:
            raise RMException('File not found')
        else:
            path = self.trash_files_path + filename
            if os.path.isfile(path):
                os.remove(path)
            elif os.path.isdir(path):
                shutil.rmtree(path)
            if transmission is not None:
                try:
                    index = transmission.filenames.index(filename)
                    del transmission.files[index]
                    del transmission.filenames[index]
                finally:
                    return

    def _restore_by_info(self, file_info, to_current_dir=False):
        """
        Restore the file using the info from the info file
        """
        path_to_restore_from = os.path.join(self.trash_files_path, file_info['Trash name'])
        if to_current_dir:
            path_to_restore = os.path.join(os.getcwd(), file_info['Trash name'])
        else:
            path_to_restore = file_info['Restore path']
        if not os.path.exists(path_to_restore):
            os.rename(path_to_restore_from, path_to_restore)
        else:
            os.remove(path_to_restore)
            os.rename(path_to_restore_from, path_to_restore)

    def _delete_files_info(self):
        """
        Delete the information about the file from the file info
        """
        with open(self.info_file, 'r') as csv_file:
            reader = csv.DictReader(csv_file)
            files_info = [row for row in reader]

        deleted_files_count = 0

        for file_info in files_info:
            file_path_in_trash = os.path.join(self.trash_path, file_info['Trash name'])
            if not os.path.exists(file_path_in_trash):
                deleted_files_count += 1
                files_info.remove(file_info)

        with open(self.info_file, 'w') as f:
            writer = csv.DictWriter(f, self.info_file_header)
            writer.writeheader()
            writer.writerows(files_info)

        return deleted_files_count

    def _delete_by_size(self, files_info, transmission):
        """
        Delete all the files from trash that are bigger than maxsize defined for trash
        """
        self.logger.info('Autodeletion by size..')

        files_info = sorted(files_info, key=lambda file_info: file_info['Size'], reverse=True)

        while files_info:
            if int(files_info[0]['Size']) > int(self.maxsize):
                self._remove_file_from_trash(files_info[0]['Trash name'], transmission)
                files_info.remove(files_info[0])
            else:
                break

        self.logger.info('Autodeletion is completed')
        return files_info

    def _delete_by_time(self, files_info, transmission):
        """
        Delete all the files from trash that were deleted more than maxtime defined for trash
        """
        self.logger.info('Autodeletion by time..')
        files_info = sorted(files_info, key=lambda file_info: file_info['Deletion time'])

        while files_info:
            deletion_time = files_info[0]['Deletion time']
            if (datetime.now() - datetime.strptime(deletion_time, '%H:%M:%S %d.%m.%y')).total_seconds()\
                    > int(self.maxtime):
                self._remove_file_from_trash(files_info[0]['Trash name'], transmission)
                files_info.remove(files_info[0])
            else:
                break
        self.logger.info('Autodeletion is completed')

    def _delete_transmissions_by_time(self, files_info):
        """
        Delete all the files from trash specified in transmissions that were added more than maxtime defined for trash
        """
        self.logger.info('Autodeletion by time..')
        files_info = sorted(files_info, key=lambda file_info: file_info['Deletion time'])

        for transmission in self.transmissions:
            if (datetime.now() - transmission.date).total_seconds() \
                    > int(self.maxtime):
                for filename in transmission.filenames:
                    file_info_to_remove = filter(lambda info: info['Trash name'] == filename, files_info)
                    self._remove_file_from_trash(filename, transmission)
                    files_info.remove(file_info_to_remove[0])
        self.logger.info('Autodeletion is completed')

    def _delete_by_space(self, files_info, transmission):
        """
        Delete some files to get enough space
        """
        self.logger.info('Autodeletion to get enough space..')
        files_info = sorted(files_info, key=lambda file_info: file_info['Deletion time'])

        while self._get_size() > self.given_space:
            self._remove_file_from_trash(files_info[0]['Trash name'], transmission)
            files_info.remove(files_info[0])
        self.logger.info('Autodeletion is completed')

    def _get_size(self):
        """
        Get the total size of the trash
        """
        total_size = 0
        for dir_path, dir_names, file_names in os.walk(self.trash_path):
            for f in file_names:
                file_path = os.path.join(dir_path, f)
                total_size += os.path.getsize(file_path)

        return total_size

    def is_in_trash(self, filename):
        """
        Checks if the file with the filename is in the trash
        """
        with open(self.info_file, 'r') as file_with_info:
            reader = csv.DictReader(file_with_info)
            file_info = [row for row in reader]

        file_info_to_restore = filter(lambda info: info['Name'] == filename, file_info)
        if not file_info_to_restore:
            file_info_to_restore = filter(lambda info: info['Trash name'] == filename, file_info)
            if not file_info_to_restore:
                return False
        return True

    def init_transmissions(self):
        """
        Transmissions initialization
        """
        self.path_transmissions = os.path.join(self.trash_path, '.transmissions')
        if not os.path.exists(self.path_transmissions):
            os.mknod(self.path_transmissions)
        try:
            with open(self.path_transmissions, 'r') as file:
                self.transmissions = pickle.load(file)
        except EOFError:
            self.transmissions = list()

    def get_transmission(self, transmission_id):
        """
        Get the transmission by its ID
        """
        transmissions = [tr for tr in self.transmissions if str(tr.id).startswith(str(transmission_id))]

        if len(transmissions) > 1:
            raise RMException(transmission_id + ' there are too many transmissions')
        elif len(transmissions) == 0:
            raise RMException(transmission_id + ' transmission not found')
        else:
            return transmissions[0]

    def load_transmissions(self):
        """
        Load the transmission from the file
        """
        with open(self.path_transmissions, 'w') as transmissions_file:
            pickle.dump(self.transmissions, transmissions_file)
