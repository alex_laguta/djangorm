import logging
import os
import re
import multiprocessing

import src.policy as policy
from src.file import File
from src.smrmexceptions import *
from src.transmission import Transmission

logger = logging.getLogger('SmartRM_logger')


def remove(path, lock, trash=None, dry_run=False):
    """
    Checks is it a file or a directory
    """
    if not os.path.exists(path):
        raise RMException(path + 'File not found')
    if os.path.isfile(path):
        return remove_file(path, lock, trash, dry_run=dry_run)
    if os.path.isdir(path):
        return remove_tree(path, lock, trash, dry_run=dry_run)


def remove_file(path, lock, trash=None, transmission=None, dry_run=False):
    """
    Remove chosen file
    """
    if not transmission:
        transmission = Transmission()
    if trash:
        if not dry_run:
            filename_in_trash = trash.move_to_trash(path, lock)
            transmission.add_file(File(path), filename_in_trash)
        elif path.startswith(trash.trash_path):
            raise RMException('Try to delete files in trash')
        elif not os.access(path, os.R_OK):
            raise RMException(path + ' permission denied')
        else:
            transmission.add_file(File(path), File(path).name)
    else:
        raise RMException('No trash to proceed the operation')

    logger.info(path + ' successfully deleted')
    return transmission


def remove_directory(path, lock, trash=None, transmission=None, dry_run=False):
    """
    Remove an empty directory
    """
    print dry_run
    if os.listdir(path):
        raise RMException(path + ' Directory is not empty')
    else:
        transmission = remove_file(path, lock, trash, transmission, dry_run)
        return transmission


def remove_tree(path, lock, trash=None, dry_run=False, silent=False, transmission=None):
    """
    Remove the directory with all the files it contains
    """
    if not transmission:
        transmission = Transmission()

    for root, dirs, files in os.walk(path):
        for file in files:
            try:
                subpath = os.path.join(root, file)
                remove_file(subpath, lock, trash, transmission, dry_run)
            except RMException as exc:
                logger.error(exc.message)
                if not silent:
                    print exc.message
                raise

        for directory in dirs:
            subpath = os.path.join(root, directory)
            if os.path.exists(subpath) and not os.access(subpath, os.R_OK):
                raise RMException(subpath + 'Permission denied')
            remove_tree(subpath, lock, trash, dry_run, silent, transmission)

        try:
            remove_directory(root, lock, trash, transmission, dry_run)
        except RMException as exc:
            logger.error(exc.message)
            if not silent:
                print exc.message

    return transmission


def remove_by_regex(path, lock, pattern, trash=None, dry_run=False, silent=False):
    """
    Remove files matching the regex
    """
    regex_transmission = None
    for root, dirs, files in os.walk(path):
        for filename in files:
            print filename
            subpath = os.path.join(root, filename)
            if _match_regex(filename, pattern):
                try:
                    regex_transmission = remove_file(subpath, lock, trash, regex_transmission, dry_run)
                except RMException as exc:
                    logger.error(exc.message)
                    if not silent:
                        print exc.message
                    raise
    return regex_transmission


def restore(restore_transmission_id, trash=None, dry_run=False, choice_policy='replace'):
    """
    Restore all the files that were added in the chosen transmission
    """

    try:
        restore_transmission = trash.get_transmission(restore_transmission_id)
        restore_transmission.files = restore_transmission.files[::-1]
        restore_transmission.filenames = restore_transmission.filenames[::-1]
        logger.info('Transmission ' + str(restore_transmission.id) + ' is restoring...')
        check_collisions = policy.restore_policy(choice_policy)
        check_collisions(restore_transmission)
        print restore_transmission.filenames

        for name in restore_transmission.filenames:
            restore_file(name, trash, dry_run, restore_transmission)
        logger.info('Transmission ' + str(restore_transmission.id) + ' restored successfully')
    except RMException as exc:
        logger.error(exc.message)
        raise


def restore_file(filename, trash=None, dry_run=False, restore_transmission=None):
    """
    Restore the file by its name in the trash
    """
    try:
        if not dry_run:
            trash.restore(filename)
        else:
            logger.info(filename + ' Trying to restore')

            if restore_transmission is not None:
                index = restore_transmission.filenames.index(filename)
                del restore_transmission.files[index]
                del restore_transmission.filenames[index]

            logger.info(filename + ' Restored successfully')
    except RMException as exc:
        logger.error(exc.message)
        raise


def set_remove_policy(name):
    """
    Set the remove policy
    """
    global remove_directory
    global remove_file
    remove_directory = policy.remove_policy(name)(remove_directory)
    remove_file = policy.remove_policy(name)(remove_file)


def _match_regex(filename, pattern):
    """
    Checks if the name is matching the regex pattern
    """
    res = re.match(pattern, os.path.basename(filename))
    if res is not None:
        res = res.group(0)

    return res == os.path.basename(filename)


def multi_remove(trash, tasks_queue, regex, dry_run):
    """
    Remove using multiprocessing
    """

    try:

        tasks = multiprocessing.JoinableQueue()
        res_transmissions = multiprocessing.Queue()
        lock = multiprocessing.Lock()

        for task in tasks_queue:
            tasks.put(task.path)
        for i in xrange(10):
            tasks.put(None)

        consumers = [Consumer(tasks, res_transmissions, trash, lock, regex, dry_run) for i in xrange(10)]
        for cons in consumers:
            cons.start()

        tasks.join()

        result_transmission = Transmission()
        for i in range(len(tasks_queue)):
            tr = res_transmissions.get()
            if isinstance(tr, RMException):
                raise tr
            if tr is None:
                continue
            for file in tr.files:
                result_transmission.files.append(file)
            for filename in tr.filenames:
                result_transmission.filenames.append(filename)

        trash.transmissions.append(result_transmission)
        trash.load_transmissions()

        return result_transmission.id

    except RMException as exc:
        raise exc
    except Exception:
        raise Exception


class Consumer(multiprocessing.Process):

    def __init__(self, task_queue, transmission_queue, trash, lock, regex, dry_run):
        multiprocessing.Process.__init__(self)
        self.task_queue = task_queue
        self.transmission_queue = transmission_queue
        self.trash = trash
        self.lock = lock
        self.regex = regex
        self.dry_run = dry_run

    def run(self):
        try:
            while True:
                next_task = self.task_queue.get()
                if next_task is None:
                    self.task_queue.task_done()
                    break
                if self.regex:
                    transmission = remove_by_regex(next_task, self.lock, str(self.regex), self.trash, self.dry_run)
                else:
                    transmission = remove(next_task, self.lock, self.trash, self.dry_run)
                self.task_queue.task_done()
                self.transmission_queue.put(transmission)
            return
        except RMException as exc:
            self.task_queue.task_done()
            while True:
                next_task = self.task_queue.get()
                if next_task is None:
                    self.task_queue.task_done()
                    break
                self.task_queue.task_done()
            self.transmission_queue.put(exc)

