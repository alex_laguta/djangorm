# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from src.trash import Trash as tr
import os


class Trash(models.Model):
    RESTORE_POLICIES = (('replace', 'replace'), ('cancel','cancel'))
    name = models.CharField(max_length=200)
    path = models.CharField(max_length=250, default=os.path.expanduser('~/trash'))
    time_created = models.DateTimeField(default=timezone.now)
    policy = models.CharField(max_length=50, default='replace', choices=RESTORE_POLICIES)
    auto_deletion = models.BooleanField(default=False)
    auto_space = models.PositiveIntegerField(default=0)
    auto_time = models.PositiveIntegerField(default=0)
    in_progress = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def trash_files(self):
        trash_path = os.path.join(self.path, 'files/')
        files = []
        if os.path.exists(trash_path):
            for name in os.listdir(trash_path):
                files.append(os.path.join(trash_path, name))
        return files

    def clear_trash(self):
        trash_path = os.path.join(self.path, 'files/')
        if os.path.exists(trash_path):
            trash = tr(path=self.path)
            trash.clear_trash()


class Task(models.Model):
    READY = 'Ready'
    OK = 'OK'
    ERROR = 'Error'
    WAITING = 'Waiting'
    STATUS_CHOICES = ((READY, 'Ready'), (OK, 'OK'), (ERROR, 'Error'), (WAITING, 'Waiting'))
    name = models.CharField(max_length=200)
    time_created = models.DateTimeField(default=timezone.now)
    trash = models.ForeignKey(Trash, on_delete=models.CASCADE)
    status = models.CharField(choices=STATUS_CHOICES, max_length=200, default='Ready')
    previous_status = models.CharField(choices=STATUS_CHOICES, max_length=200, default='Ready')
    regex = models.CharField(max_length=200, blank=True, default='')
    transmission = models.CharField(max_length=200, null=True, blank=True)
    dry_run = models.BooleanField(default=False)
    error_message = models.CharField(max_length=200, blank=True, default='')

    def __str__(self):
        return self.name


class File(models.Model):
    path = models.CharField(max_length=150, default='')
    task = models.ForeignKey(Task, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.path



