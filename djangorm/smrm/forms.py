from django import forms
from smrm.models import Trash, Task, File


class TrashForm(forms.ModelForm):

    class Meta:
        model = Trash
        fields = ['name', 'path', 'policy', 'auto_deletion', 'auto_space', 'auto_time']


class TrashEditForm(forms.ModelForm):

    class Meta:
        model = Trash
        fields = ['name', 'policy', 'auto_deletion', 'auto_space', 'auto_time']


class TaskForm(forms.ModelForm):

    class Meta:
        model = Task
        fields = ['name', 'trash', 'regex', 'dry_run']


class FileForm(forms.ModelForm):

    class Meta:
        model = File
        fields = ['path', 'task']