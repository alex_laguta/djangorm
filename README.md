# README #

### What is this repository for? ###

This repository contains DjangoRM, the upgraded version of SmartRM. SmartRM is a tool for removing your files to the trash and restoring them from the trash in a smart way (using different policies, regular expressions and a customization of the trash). It's an improved version of linux built-in remove function. DjangoRM contains the web interface for SmartRM.

### How do I get set up? ###

To run the DjangoRM go to the folder with the project and use this command in the terminal:

$ python manage.py runserver

### Who do I talk to? ###

Alex Laguta 

e-mail: lehalag@gmail.com